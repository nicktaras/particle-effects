# README #

Experimental Project with particles, morphing multiple geometric 3D objects into large geometric shapes. 

### What is this repository for? ###

* R & D
* Version 0.1

### How do I get set up? ###

* Pull down the repo.
* Open the local copy of the repo, look in 'assets/' for the file 'PE.unity' 

### Contribution guidelines ###

* Make something cool.

### Who do I talk to? ###

* Repo owner or admin - nicktaras / nicktaras@hotmail.co.uk
* Other community or team contact / tbc.