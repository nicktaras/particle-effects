﻿#pragma strict

public var ballObj : Rigidbody; 
public var boundsMinObj : Transform; // Plane in Scene of where Particles will be out of the view. TODO: remove or re-position in view.
public var boundsMaxObj : Transform; // Plane in Scene of where Particles will be spawned from. 

public class InitScene extends MonoBehaviour {

    public class InitParticles {

        public var particleNum : int;

        public function InitParticles(ParticleNum : int) {
            particleNum = ParticleNum;
        }

        //Constructor
        public function InitParticles(){
            particleNum = 100;
        }

    }
    
    public class InitSphere {}

    public var initParticles : InitParticles = new InitParticles();

    function Start (){
    
		for ( var i:int = 0; i < initParticles.particleNum; i ++ ) {

			var phi : float;
			var theta : float;
			var randomX : float; 
			var randomY : float;
			var randomZ : float;
			
			phi     = Mathf.Acos( -0.99 + ( 1.99 * i ) / initParticles.particleNum );
			theta   = Mathf.Sqrt( initParticles.particleNum * Mathf.PI ) * phi;
			randomX = 10 * Mathf.Cos( theta ) * Mathf.Sin( phi );
			randomY = 10 * Mathf.Sin( theta ) * Mathf.Sin( phi );
			randomZ = 10 * Mathf.Cos( phi ) + boundsMaxObj.GetComponent(MeshRenderer).bounds.min.z - 50;
				
//			randomX = Random.Range(boundsMinObj.GetComponent(MeshRenderer).bounds.min.x, boundsMinObj.GetComponent(MeshRenderer).bounds.max.x);
//    		randomY = Random.Range(boundsMinObj.GetComponent(MeshRenderer).bounds.min.y, boundsMinObj.GetComponent(MeshRenderer).bounds.max.y);
//    		randomZ = Random.Range(boundsMaxObj.GetComponent(MeshRenderer).bounds.min.z-1, boundsMaxObj.GetComponent(MeshRenderer).bounds.max.z+1);

			var randomPositionBetweenBounds : Vector3 = Vector3(randomX, randomY, randomZ);

			Instantiate(ballObj, randomPositionBetweenBounds, Quaternion.identity);

		}

    }

    function Update (){}

}
